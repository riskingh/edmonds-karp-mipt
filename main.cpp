#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

const long long MAX_CAPACITY = 100;

struct Graph {
    std::vector< std::vector<long long> > capacity;
    std::vector< std::vector<long long> > flow;

    Graph(std::vector< std::vector<long long> > capacity)
    : capacity(capacity) {
        flow.resize(capacity.size());
        for (unsigned int index = 0; index < capacity.size(); ++index)
            flow[index].resize(capacity.size(), 0);
    }

    long long residual(unsigned int firstVertex, unsigned int secondVertex) {
        return capacity[firstVertex][secondVertex] - flow[firstVertex][secondVertex];
    }

    unsigned int size() {
        return capacity.size();
    }
};

long long findFlow(Graph &_graph, unsigned int _start, unsigned int _finish) {
    std::queue<unsigned int> bfsQueue;
    bfsQueue.push(_start);

    std::vector<int> prev(_graph.size(), -1);
    std::vector<int> dist(_graph.size(), -1);
    dist[_start] = 0;
    int currentVertex, nextVertex;
    while (!bfsQueue.empty() && dist[_finish] == -1) {
        currentVertex = bfsQueue.front();
        bfsQueue.pop();
        for (nextVertex = 0; nextVertex < _graph.size(); ++nextVertex) {
            if (currentVertex == nextVertex || _graph.residual(currentVertex, nextVertex) <= 0 || dist[nextVertex] != -1)
                continue;
            dist[nextVertex] = dist[currentVertex] + 1;
            prev[nextVertex] = currentVertex;
            bfsQueue.push(nextVertex);
        }
    }

    if (dist[_finish] == -1)
        return 0;

    long long flow = MAX_CAPACITY;
    for (currentVertex = _finish; prev[currentVertex] != -1; currentVertex = prev[currentVertex])
        flow = std::min(flow, _graph.residual(prev[currentVertex], currentVertex));
    for (currentVertex = _finish; prev[currentVertex] != -1; currentVertex = prev[currentVertex]) {
        _graph.flow[prev[currentVertex]][currentVertex] += flow;
        _graph.flow[currentVertex][prev[currentVertex]] -= flow;
    }

    return flow;
}

long long EdmondsKarp(Graph &_graph, unsigned int _start, unsigned int _finish) {
    long long flow = 0, newFlow = findFlow(_graph, _start, _finish);
    while (newFlow > 0) {
        flow += newFlow;
        newFlow = findFlow(_graph, _start, _finish);
    }
    return flow;
}

int main() {
    freopen("input.txt", "r", stdin);

    int n, m;
    std::cin >> n >> m;

    std::vector< std::vector<long long> > capacity(n);
    for (unsigned int index = 0; index < n; ++index)
        capacity[index].resize(n, 0);

    int a, b, c;
    for (unsigned int index = 0; index < m; ++index) {
        std::cin >> a >> b >> c;
        a--;
        b--;
        capacity[a][b] = c;
    }

    Graph graph(capacity);
    std::cout << EdmondsKarp(graph, 0, n - 1) << "\n";
    return 0;
}
